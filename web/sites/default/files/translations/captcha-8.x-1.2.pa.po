# Punjabi translation of CAPTCHA (8.x-1.2)
# Copyright (c) 2021 by the Punjabi translation team
#
msgid ""
msgstr ""
"Project-Id-Version: CAPTCHA (8.x-1.2)\n"
"POT-Creation-Date: 2021-04-29 05:46+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Language-Team: Punjabi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

msgid "Delete"
msgstr "ਹਟਾਓ"
msgid "Text color"
msgstr "ਟੈਕਸਟ ਰੰਗ"
