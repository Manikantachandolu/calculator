<?php
namespace Drupal\drupal_simple_form\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
/**
 * Our simple form class.
 */
class SimpleForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drupal_simple_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $genderOptions = array(
      '0' => 'Select Gender',
      'Male' => 'Male',
      'Female' => 'Female',
      'Other' => 'Other'
);
  $form['first_name'] = array(
    '#type' => 'textfield',
    '#title' => $this->t('First Name:'),
    //'#required' => TRUE,
  );
  $form['last_name'] = array(
    '#type' => 'textfield',
    '#title' => $this->t('Last Name:'),
    //'#required' => TRUE,
  );
  $form['email'] = array(
    '#type' => 'email',
    '#title' => $this->t('Email ID:'),
    //'#required' => TRUE,
  );
  $form['candidate_number'] = array (
    '#type' => 'tel',
    '#title' => $this->t('Mobile no'),
  );
  $form['candidate_dob'] = array (
    '#type' => 'date',
    '#title' =>$this->t('DOB'),
    //'#required' => TRUE,
  );
  $form['gender'] = array(
    '#type' => 'select',
    '#title' => 'Gender',
    '#options' =>$genderOptions
  );
  $form['candidate_confirmation'] = array (
    '#type' => 'radios',
    '#title' => ('Are you above 18 years old?'),
    '#options' => array(
      'Yes' =>$this->t('Yes'),
      'No' =>$this->t('No')
    ),
  );
  $form['Address1'] = array (
    '#type' => 'textfield',
    '#title' =>$this->t('Address Line1'),
    //'#required' => TRUE,
  );
  $form['Address2'] = array(
    '#type' => 'textfield',
      '#title' => $this->t('Address Line2'),
      //'#required' => TRUE
       );
       $form['country'] = array(
        '#type' => 'textfield',
          '#title' => $this->t('country'),
          //'#required' => TRUE
           );
  $form['Address'] = array (
    '#type' => 'textfield',
    '#title' => $this->t('State'),
   // '#required' => TRUE,
  );
  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => $this->t('Submit'),
    '#button_type' => 'primary',
  );
    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $name = $form_state->getValue('first_name');
    if(trim($name) == ''){
      $form_state->setErrorByName('First_name', $this->t('First name field is required'));
     }
     if(!preg_match('/^([a-zA-Z]+\s)*[a-zA-Z]+$/' ,$form_state->getvalue('first_name'))){
      $form_state->setErrorByName('First_name', $this->t('first name should not contain special Characters'));
     }

      $name = $form_state->getValue('last_name');
      if(trim($name) == ''){
        $form_state->setErrorByName('Last_name', $this->t('last name field is required'));
      }
      if(!preg_match('/^([a-zA-Z]+\s)*[a-zA-Z]+$/' ,$form_state->getvalue('last_name'))){
        $form_state->setErrorByName('Last_name', $this->t('last name should not contain special Characters'));
       }

    if (strlen($form_state->getValue('candidate_number')) < 10) {
      $form_state->setErrorByName('candidate_number', $this->t('Mobile number required.'));
    }
    $candidate_number= $form_state->getValue('candidate_number');
    if (strlen($candidate_number) > 0 && !preg_match('/^\d{10}$/', $candidate_number))
    $form_state->setErrorByName('candidate_number', $this->t('Mobile number should not contain characters'));


    if($form_state->getValue('gender') == '0'){
      $form_state->setErrorByName('gender', $this->t('Gender field is required'));
    }
    //$name = $form_state->getValue('Address1');
    //if(trim($name) == ''){
      //$form_state->setErrorByName('Address Line1', $this->t('Address Line1 field is required'));
    //} $name = $form_state->getValue('Address2');
    //if(trim($name) == ''){
      //$form_state->setErrorByName('Address Line2', $this->t('Address Line2 field is required'));
    //}
    //$name = $form_state->getValue('country');
    //if(trim($name) == ''){
      //$form_state->setErrorByName('country', $this->t('country field is required'));
    //} $name = $form_state->getValue('Address');
    //if(trim($name) == ''){
      //$form_state->setErrorByName('state', $this->t('state field is required'));
    //}
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    //drupal_set_message($this->t('@can_name ,Your application is being submitted!', array('@can_name' => $form_state->getValue('candidate_name'))));
     //foreach ($form_state->getValues() as $key => $value) {
       //drupal_set_message($key . ': ' . $value);
       $postData = $form_state->getValue();
       echo "<pre>";
       print_r($postData);
       echo "</pre>";
       exit;
     }
    }
  //}
?>